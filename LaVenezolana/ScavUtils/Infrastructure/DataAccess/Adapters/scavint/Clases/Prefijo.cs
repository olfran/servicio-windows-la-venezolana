﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace scavint.Clases
{
    public class Prefijo
    {
        private readonly string _connectionString;

        public Prefijo(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Devuelve una lista de los prefijos disponibles para un catálogo
        /// </summary>
        /// <param name="idCatalogo"></param>
        /// <returns></returns>
        public List<PrefijoPropiedades> getListaPrefijos(long idCatalogo)
        {
            try
            {
                using (OleDbConnection cn = new OleDbConnection(_connectionString))
                {
                    cn.Open();

                    PrefijoPropiedades prefijoPropiedades;

                    List<PrefijoPropiedades> lista = new List<PrefijoPropiedades>();

                    OleDbCommand sqlCmd = new OleDbCommand("SELECT PREFIJO,ID_PREFIJO,DIGITOS_ANO,DIGITOS_CONSEC FROM PREFIJO WHERE ID_CATALOGO=" + idCatalogo, cn);
                    OleDbDataReader sqlReader = sqlCmd.ExecuteReader();
                    if (sqlReader.HasRows)
                    {
                        while (sqlReader.Read())
                        {
                            prefijoPropiedades = new PrefijoPropiedades();
                            prefijoPropiedades.idPrefijo = long.Parse(sqlReader.GetValue(1).ToString());
                            prefijoPropiedades.nombrePrefijo = sqlReader.GetValue(0).ToString();
                            prefijoPropiedades.digitosAno = Utilidades.getValorIntDeString(sqlReader.GetValue(2).ToString());
                            prefijoPropiedades.digitosConsecutivo = Utilidades.getValorIntDeString(sqlReader.GetValue(3).ToString());
                            lista.Add(prefijoPropiedades);

                        }
                        return lista;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                List<string> lista = new List<string>();
                lista.Add("Sin conexión a la BD");
                lista.Add(e.Message);
                return null;
            }
        }
        /// <summary>
        /// Devuelve un prefijo particular a partir del idPrefijo
        /// </summary>
        /// <param name="idPrefijo"></param>
        /// <returns></returns>
        public PrefijoPropiedades getPrefijo(long idPrefijo) {
            try
            {
                using (OleDbConnection cn = new OleDbConnection(_connectionString))
                {
                    cn.Open();
                    OleDbCommand sqlCmd = new OleDbCommand("SELECT PREFIJO,DIGITOS_ANO,DIGITOS_CONSEC,ID_CATALOGO FROM PREFIJO WHERE ID_PREFIJO=" + idPrefijo, cn);
                    OleDbDataReader sqlReader = sqlCmd.ExecuteReader();
                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();
                        PrefijoPropiedades prefijo = new PrefijoPropiedades();
                        prefijo.idPrefijo = idPrefijo;
                        prefijo.nombrePrefijo = sqlReader.GetValue(0).ToString();
                        prefijo.digitosAno = int.Parse(sqlReader.GetValue(1).ToString());
                        prefijo.digitosConsecutivo = int.Parse(sqlReader.GetValue(2).ToString());
                        prefijo.idCatalogo = Utilidades.getValorLongDeString(sqlReader.GetValue(3).ToString());
                        sqlReader.Dispose();
                        return prefijo;
                    }
                    else
                    {
                        sqlReader.Dispose();
                        return null;
                    }
                }
                
            }
            catch { return null; }
        }
        /// <summary>
        /// Establece el largo del consecutivo de un numref
        /// </summary>
        /// <param name="prefijo"></param>
        /// <returns></returns>
        public string setLargoConsecutivo(PrefijoPropiedades prefijo,int largo)
        {
            string resultado = "";
            try
            {
                string sql = "";
                using (OleDbConnection cn = new OleDbConnection(_connectionString))
                {
                    cn.Open();
                    int tamanoPrefijo = prefijo.digitosAno + prefijo.nombrePrefijo.Length + 3;
                    int tamanoPrefijo2 = tamanoPrefijo;
                    string ceros = "'" +  Utilidades.rellenaCeros("", largo - prefijo.digitosConsecutivo) + "'"; 
                    if (largo < prefijo.digitosConsecutivo) { 
                        //si el nuevo tamaño es menor
                        int cuanto = prefijo.digitosConsecutivo - largo;
                        tamanoPrefijo2 = tamanoPrefijo2 + cuanto;
                        ceros="";
                    }

                    //quitar:
                    //UPDATE CATALOGO_6 SET CATALOGO_6.NUMREF = SUBSTRING(NUMREF,0,6) + SUBSTRING(NUMREF,7,5) WHERE NUMREF LIKE 'G-%'
                    OleDbTransaction myTrans;
                    myTrans = cn.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                    OleDbCommand sqlCmd = cn.CreateCommand();//new OleDbCommand(null, cn);
                    sqlCmd.Connection = cn;
                    sqlCmd.Transaction = myTrans;
                    sql = "UPDATE CATALOGO_" + prefijo.idCatalogo + " SET NUMREF = SUBSTRING(NUMREF,0," + tamanoPrefijo + ") + " + ceros + " + SUBSTRING(NUMREF," + tamanoPrefijo2 + "," + prefijo.digitosConsecutivo + ") WHERE NUMREF LIKE ?";
                    sqlCmd.Parameters.AddWithValue("@p1", prefijo.nombrePrefijo + "-%");
                    sqlCmd.CommandText = sql;
                    sqlCmd.ExecuteNonQuery();
                    sql = "UPDATE PREFIJO SET DIGITOS_CONSEC=" + largo + " WHERE ID_PREFIJO=" + prefijo.idPrefijo;
                    sqlCmd.CommandText = sql;
                    sqlCmd.ExecuteNonQuery();
                    myTrans.Commit();
                    resultado = "Datos guardados con éxito";
                }

            }
            catch (Exception err) { resultado = err.Message; }
            return resultado;
        }
    }
}