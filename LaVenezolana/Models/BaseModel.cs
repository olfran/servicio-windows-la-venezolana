﻿namespace LaVenezolana.Models
{
    /// <summary>
    /// Modelo base
    /// </summary>
    public class BaseModel
    {
        /// <summary>
        /// Línea proveniente del archivo de texto se supone que siempre 
        /// la información estará disponible en un archivo txt
        public string TextLine { get; set; }
    }
}
