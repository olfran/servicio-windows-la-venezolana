﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Text;

namespace scavint.Clases
{
    public class Formulario
    {
        private readonly string _connectionString;

        public Formulario(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Devuelve la lista de formularios asociados a un catálogo
        /// </summary>
        /// <param name="idCatalogo"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public List<FormularioPropiedades> getFormularioLista(long idCatalogo, int tipo = 0)
        {
            try
            {
                List<FormularioPropiedades> lista = new List<FormularioPropiedades>();
                FormularioPropiedades formulariopropiedades;
                using (OleDbConnection cn = new OleDbConnection(_connectionString))
                {
                    cn.Open();
                    string SQL = "SELECT FORMULARIOS.NOMBRE_FORMULARIO, FORMULARIOS.ID_FORMULARIO FROM FORMULARIOS WHERE ID_CATALOGO=" + idCatalogo + " AND TIPO_FORMULARIO=" + tipo + " ORDER BY NOMBRE_FORMULARIO";
                    if (tipo == 0)
                    {
                        SQL = "SELECT FORMULARIOS.NOMBRE_FORMULARIO, FORMULARIOS.ID_FORMULARIO FROM FORMULARIOS WHERE ID_CATALOGO=" + idCatalogo + " ORDER BY NOMBRE_FORMULARIO";
                    }
                    OleDbCommand sqlCmd = new OleDbCommand(SQL, cn);
                    OleDbDataReader sqlReader = sqlCmd.ExecuteReader();
                    if (sqlReader.HasRows)
                    {
                        while (sqlReader.Read())
                        {
                            formulariopropiedades = new FormularioPropiedades();
                            formulariopropiedades.nombreFormulario = sqlReader.GetValue(0).ToString();
                            formulariopropiedades.idFormulario = Utilidades.getValorLongDeString(sqlReader.GetValue(1).ToString());
                            lista.Add(formulariopropiedades);
                        }
                        return lista;
                    }
                    else
                    {
                        return lista;
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        public List<FormularioDetallePropiedades> getFormularioDetalle(long idCatalogo)
        {
            return getFormularioDetalleEjecuta(idCatalogo, 0);
        }
        public List<FormularioDetallePropiedades> getFormularioDetalle(long idCatalogo, long idFormulario)
        {
            return getFormularioDetalleEjecuta(idCatalogo, idFormulario);
        }
        /// <summary>
        /// Devuelve la estructura de un formulario
        /// </summary>
        /// <param name="idCatalogo"></param>
        /// <param name="soloCamposReales">Sólo trae los campos tipo texto, número, fecha y memo</param>
        /// <param name="soloBuzon"></param>
        /// <returns></returns>
        public List<FormularioDetallePropiedades> getFormularioDetalle(long idCatalogo, bool soloCamposReales, bool soloBuzon)
        {
            return getFormularioDetalleEjecuta(idCatalogo, 0, soloCamposReales, soloBuzon);
        }
        public List<FormularioDetallePropiedades> getFormularioDetalle(long idCatalogo, long idFormulario, bool soloCamposReales, bool soloBuzon)
        {
            return getFormularioDetalleEjecuta(idCatalogo, idFormulario, soloCamposReales, soloBuzon);
        }
        public List<FormularioDetallePropiedades> getFormularioDetalle(long idCatalogo, long idFormulario, bool soloCamposReales)
        {
            return getFormularioDetalleEjecuta(idCatalogo, idFormulario, soloCamposReales);
        }
        private List<FormularioDetallePropiedades> getFormularioDetalleEjecuta(long idCatalogo, long idFormulario, bool soloCamposReales = false, bool soloBuzon = false)
        {
            List<FormularioDetallePropiedades> lista = new List<FormularioDetallePropiedades>();
            try
            {
                FormularioDetallePropiedades formularioDetalle;
                using (OleDbConnection cn = new OleDbConnection(_connectionString))
                {
                    cn.Open();
                    string idFormularioAux = "";
                    if (idFormulario == 0) { idFormularioAux = " IS NULL"; } else { idFormularioAux = "=" + idFormulario.ToString(); }
                    string SQL = "SELECT" +
                    " CATALOGO_ESTRUCTURA.NOMBRE_ELEMENTO_CAT_STR," + //0
                    " FORMULARIO_DETALLE.POS_X," + //1
                    " FORMULARIO_DETALLE.POS_Y," + //2
                    " FORMULARIO_DETALLE.TAMANO_X," + //3
                    " FORMULARIO_DETALLE.TAMANO_Y," + //4
                    " CATALOGO_ESTRUCTURA.TIPO_ELEMENTO," + //5
                    " CATALOGO_ESTRUCTURA.ID_CAMPO_CAT_STR," + //6
                    " FORMULARIO_DETALLE.HABILITADO," + //7
                    " CATALOGO_ESTRUCTURA.TAMANO_CAT_STR," + //8
                    " CATALOGO_ESTRUCTURA.ASUNTO_CAT_STR," + //9
                    " CATALOGO_ESTRUCTURA.VARIOS_CAT_STR," + //10
                    " CATALOGO_ESTRUCTURA.ID_TESAURO," + //11
                    " CATALOGO_ESTRUCTURA.MASCARA_CAT_STR," + //12
                    " FORMULARIO_DETALLE.NOMBRE_FUENTE_FORM_DETA," + //13
                    " FORMULARIO_DETALLE.FORMATO_FUENTE_FORM_DETA," + //14
                    " FORMULARIO_DETALLE.COLOR_FORM_DETALLE," + //15
                    " FORMULARIO_DETALLE.TIPO_ELEM," + //16
                    " FORMULARIO_DETALLE.REQUERIDO," + //17
                    " CATALOGO_ESTRUCTURA.AUTOLLENADO," + //18
                    " FORMULARIO_DETALLE.VALOR_DEFECTO," + //19
                    " FORMULARIO_DETALLE.CODIGO_BARRAS" + //20
                    " FROM FORMULARIO_DETALLE INNER JOIN" +
                    " CATALOGO_ESTRUCTURA ON FORMULARIO_DETALLE.ID_CAMPO_CAT_STR=CATALOGO_ESTRUCTURA.ID_CAMPO_CAT_STR" +
                    " WHERE FORMULARIO_DETALLE.ID_FORMULARIO" + idFormularioAux +
                    " AND CATALOGO_ESTRUCTURA.ID_CATALOGO=" + idCatalogo;
                    if (soloCamposReales) { SQL = SQL + " AND (CATALOGO_ESTRUCTURA.TIPO_ELEMENTO<4 OR CATALOGO_ESTRUCTURA.TIPO_ELEMENTO=10)"; }
                    if (soloBuzon) { SQL = SQL + " AND CATALOGO_ESTRUCTURA.BUZON_CAT_STR=1"; }
                    SQL = SQL + " ORDER BY FORMULARIO_DETALLE.POS_Y,FORMULARIO_DETALLE.POS_X";
                    OleDbCommand sqlCmd = new OleDbCommand(SQL, cn);
                    OleDbDataReader sqlReader = sqlCmd.ExecuteReader();
                    if (sqlReader.HasRows)
                    {
                        while (sqlReader.Read())
                        {
                            formularioDetalle = new FormularioDetallePropiedades();
                            formularioDetalle._nombreCampo = sqlReader.GetValue(0).ToString();
                            formularioDetalle._posX = long.Parse(sqlReader.GetValue(1).ToString()) / 15;
                            formularioDetalle._posY = long.Parse(sqlReader.GetValue(2).ToString()) / 15;
                            formularioDetalle._tamX = long.Parse(sqlReader.GetValue(3).ToString()) / 15;
                            formularioDetalle._tamY = long.Parse(sqlReader.GetValue(4).ToString()) / 15;
                            formularioDetalle._tipoElemento = int.Parse(sqlReader.GetValue(5).ToString());
                            formularioDetalle._idCampo = long.Parse(sqlReader.GetValue(6).ToString());
                            formularioDetalle._Habilitado = int.Parse(sqlReader.GetValue(7).ToString());
                            formularioDetalle._MaxLenght = int.Parse(sqlReader.GetValue(8).ToString());
                            formularioDetalle._asuntoTitulo = Utilidades.getVerdaderoFalso(sqlReader.GetValue(9).ToString());
                            formularioDetalle._variosTerminos = Utilidades.getVerdaderoFalso(sqlReader.GetValue(10).ToString());
                            formularioDetalle._obligatorio = Utilidades.getVerdaderoFalso(sqlReader.GetValue(17).ToString());
                            formularioDetalle._idTesauro = Utilidades.getValorLongDeString(sqlReader.GetValue(11).ToString());
                            if (sqlReader.GetValue(12) != null) { formularioDetalle._formato = Utilidades.getNomralizaMascara(sqlReader.GetValue(12).ToString()); } else { formularioDetalle._formato = ""; };

                            if (sqlReader.GetValue(13) != null) { formularioDetalle._fuenteNombre = sqlReader.GetValue(13).ToString(); } else { formularioDetalle._fuenteNombre = ""; };
                            if (sqlReader.GetValue(14) != null) { formularioDetalle._fuenteTamano = sqlReader.GetValue(14).ToString(); } else { formularioDetalle._fuenteTamano = ""; };
                            if (sqlReader.GetValue(15) != null) { formularioDetalle._fuenteColor = sqlReader.GetValue(15).ToString(); } else { formularioDetalle._fuenteColor = ""; };

                            if (sqlReader.GetValue(16) != null)
                            {
                                if (sqlReader.GetValue(16).ToString() != "")
                                {
                                    formularioDetalle._tipoElementoEtiqueta = int.Parse(sqlReader.GetValue(16).ToString());
                                }
                            }
                            formularioDetalle._Autollenado = false;
                            if (sqlReader.GetValue(18) != null)
                            {
                                formularioDetalle._Autollenado = Utilidades.getVerdaderoFalso(sqlReader.GetValue(18).ToString());
                            }
                            formularioDetalle.ValorDefecto = sqlReader.GetValue(19).ToString();
                            formularioDetalle.CodigoBarra = Utilidades.getVerdaderoFalso(sqlReader.GetValue(20).ToString());
                            lista.Add(formularioDetalle);
                        }
                        return lista;
                    }
                    else
                    {
                        return lista;
                    }
                }
            }
            catch
            {
                return lista;
            }
        }
   }
}