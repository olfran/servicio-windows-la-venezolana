﻿namespace ScavConfiguration.Interfaces
{
    public interface IScavConfigurationReader
    {
        string Get(string item);
    }
}
