﻿namespace LaVenezolana.Models
{
    /// <summary>
    /// Configuración de los IDs del proyecto
    /// </summary>
    public class LaVenezolanaConfiguracion
    {
        public long IdCatalogoSiniestros { get; set; }
        public long IdCampoEmpresa { get; set; }
        public long IdCampoSucursal { get; set; }
        public long IdCampoRamo { get; set; }
        public long IdCampoNombreAsegurado { get; set; }
        public long IdCampoCedulaAsegurado { get; set; }
        public long IdCampoNumeroPoliza { get; set; }
        public long IdCampoTipoSiniestro { get; set; }
        public long IdCampoFechaOcurrenciaSiniestro { get; set; }
        public long IdCampoNumeroSiniestro { get; set; }
        public long IdCampoEstatusSiniestro { get; set; }
        public long IdCampoFechaLiquidacionSiniestro { get; set; }
        public long IdCampoTipoProveedor { get; set; }
        public long IdCampoCodigoProveedor { get; set; }
        public long IdCampoCedulaProveedor { get; set; }
        public long IdCampoNombreProveedor { get; set; }
        public long IdCampoNumeroFactura { get; set; }
        public long IdCampoFechaLiquidacionFactura { get; set; }
        public long IdCampoEstatusFactura { get; set; }
    }
}
