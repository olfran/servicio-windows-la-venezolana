﻿namespace LaVenezolana.Models
{
    /// <summary>
    /// Representa los archivos de imágenes del FAX junto con el número de siniestro
    /// </summary>
    public class ImagenesFax
    {
        public string NumeroSiniestro { get; set; }
        public FaxFiles ArchivosFax { get; set; }
    }
}
