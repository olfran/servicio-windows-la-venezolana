﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LaVenezolana.Infrastructure.Parser;
using System.IO;
using LaVenezolana.Models;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class ParserTxtTest
    {
        const string TestString = "V-162441111     1-14-141055370\nV-22222500      1-14-141081148";
        IParser _parser;
        TextReader _reader = new StringReader(TestString);
        string[] _expectedCedula = new string[] { "V-162441111", "V-22222500" };
        string[] _expectedNumeroSiniestro = new string[] { "1-14-141055370", "1-14-141081148" };

        public ParserTxtTest()
        {
            _parser = new SiniestrosParserTxt(_reader);
        }

        [TestMethod]
        public void DeberiaRetornarLaCedulaYElNumeroSiniestro()
        {
            IList<Siniestro> result = _parser.Parse();

            Assert.AreEqual(_expectedCedula[0], result[0].Cedula);
            Assert.AreEqual(_expectedCedula[1], result[1].Cedula);

            Assert.AreEqual(_expectedNumeroSiniestro[0], result[0].NumeroSiniestro);
            Assert.AreEqual(_expectedNumeroSiniestro[1], result[1].NumeroSiniestro);

        }

        [TestMethod]
        public void DeberiaRetornarUnaListaVaciaSiElArchivoEstaVacio()
        {
            using (TextReader reader = new StringReader(""))
            {
                IParser parser = new SiniestrosParserTxt(reader);
                IList<Siniestro> result = parser.Parse();
                Assert.AreEqual(0, result.Count);
            }
        }

        [TestCleanup]
        public void TearDown()
        {
            _reader.Close();
        }
    }
}
