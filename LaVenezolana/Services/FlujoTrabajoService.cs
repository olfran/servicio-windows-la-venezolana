﻿using ScavUtils.Infrastructure.DataAccess.Adapters;

namespace LaVenezolana.Services
{
    /// <summary>
    /// Comunicarse con el servicio web de envío de documentos al flujo
    /// </summary>
    public class FlujoTrabajoService
    {
        readonly IScavAdapter _scavAdapter;

        public FlujoTrabajoService(IScavAdapter scavAdapter)
        {
            _scavAdapter = scavAdapter;
        }

        /// <summary>
        /// Permite enviar una lista de documentos a un flujo de trabajo
        /// </summary>
        public bool EnviarDocumento(long idCatalogo, long idDocumento, long idFlujo, long idUsuario)
        {
            return _scavAdapter.EnviarDocumento(idCatalogo, idDocumento, idFlujo, idUsuario);
        }
    }
}
