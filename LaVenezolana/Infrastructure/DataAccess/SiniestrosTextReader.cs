﻿using System.IO;

namespace LaVenezolana.Infrastructure.DataAccess
{
    /// <summary>
    /// Leer de un archivo de texto
    /// </summary>
    public class SiniestrosTextReader : ISiniestrosTextReader
    {
        private StreamReader _reader;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileStream">Archivo a escribir</param>
        public SiniestrosTextReader(FileStream fileStream)
        {
            _reader = new StreamReader(fileStream);
        }

        /// <summary>
        /// Leer una línea de texto
        /// </summary>
        /// <returns>Línea de texto leída</returns>
        public string ReadLine()
        {
            return _reader.ReadLine();
        }
    }
}