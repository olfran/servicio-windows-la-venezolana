﻿using System;
namespace ScavUtils.Infrastructure.Models
{
    public class Paginador
    {
        int _paginaActual;
        
        public int PaginaActual 
        {
            get { return _paginaActual;  }
            set { _paginaActual = value < 1 ? 1 : value; }
        }
        public int TamanoPagina { get; set; }

        /// <summary>
        /// Numero total de elementos a paginar COUNT(*) o similares
        /// </summary>
        public long TotalElementos { get; set; }

        public int TotalPaginas
        {
            get
            {
                try
                {
                    return (int)Math.Ceiling((double)TotalElementos / TamanoPagina);
                }
                catch
                {
                    return 1;
                }
            }
        }
        
        /// <summary>
        /// Offset pensado para las consultas en la Base de Datos
        /// </summary>
        public int Offset
        {
            get
            {
                int offsetActual = PaginaActual <= 1 ? 1 : PaginaActual;

                return (offsetActual - 1) * TamanoPagina;
            }
        }

        public Paginador()
        {
            //Defecto
            PaginaActual = 0;  //Offset
            TamanoPagina = 10; //Limit
        }

        public Paginador(int paginaActual, int tamanoPagina, long totalElementos = 0)
        {
            PaginaActual = paginaActual;
            TamanoPagina = tamanoPagina;
            TotalElementos = totalElementos;
        }
    }
}
