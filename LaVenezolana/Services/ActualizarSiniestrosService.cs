﻿using LaVenezolana.Infrastructure.Parser;
using LaVenezolana.Models;
using Logging;
using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;

namespace LaVenezolana.Services
{
    /// <summary>
    /// Servicio para generar los registros de los siniestros en la base de datos
    /// de SCAV a partir del archivo de texto
    /// </summary>
    public class ActualizarSiniestrosService
    {
        public LaVenezolanaConfiguracion Config { get; set; }

        private const string OkActualizar = "OK UPDATE - Documento con Número de Siniestro: [{0}] actualizado correctamente";
        private const string ErrorActualizar = "ERROR UPDATE - Documento con Número de Siniestro: [{0}] no pudo ser actualizado, probablemente NO exista";

        private readonly IParser<Siniestro> _parser;
        private readonly IScavDataAccess _scavDataAccess;
        private readonly ILogger _logger;

        /// <summary>
        /// Crear los siniestros en forma de Registros de SCAV
        /// </summary>
        public ActualizarSiniestrosService(IParser<Siniestro> parser, IScavDataAccess scavDataAccess, ILogger logger)
        {
            _parser = parser;
            _scavDataAccess = scavDataAccess;
            _logger = logger;
        }

        /// <summary>
        /// Actualizar los siniestros y almacenar en el log la operación
        /// </summary>
        /// <returns>Los registros no procesados o una lista vacía si todos fueron procesados correctamente</returns>
        public IList<Siniestro> ActualizarSiniestros()
        {
            IList<Siniestro> siniestros = _parser.Parse();
            IList<Siniestro> noProcesados = new List<Siniestro>();
            IList<Campo> campos = GetCampos();
            IList<Campo> camposWhere = new List<Campo>
            {
                new Campo() { Id = Config.IdCampoNumeroSiniestro }
            };

            foreach (var siniestro in siniestros)
            {
                camposWhere[0].Valor = siniestro.NumeroSiniestro;
                ActualizarValores(ref campos, siniestro);

                if (_scavDataAccess.ActualizarDocumento(campos, Config.IdCatalogoSiniestros, camposWhere))
                {
                    string ok = String.Format(OkActualizar, siniestro.NumeroSiniestro);
                    _logger.Log(ok);
                }
                else
                {
                    noProcesados.Add(siniestro);
                    string error = String.Format(ErrorActualizar, siniestro.NumeroSiniestro);
                    _logger.Log(error);
                }
            }

            return noProcesados;
        }

        private IList<Campo> GetCampos()
        {
            return new List<Campo>()
            {
                new Campo() { Id = Config.IdCampoEmpresa },
                new Campo() { Id = Config.IdCampoSucursal },
                new Campo() { Id = Config.IdCampoRamo },
                new Campo() { Id = Config.IdCampoNombreAsegurado },
                new Campo() { Id = Config.IdCampoCedulaAsegurado },
                new Campo() { Id = Config.IdCampoNumeroPoliza },
                new Campo() { Id = Config.IdCampoTipoSiniestro },
                new Campo() { Id = Config.IdCampoFechaOcurrenciaSiniestro, Tipo = TipoCampo.Fecha },
                new Campo() { Id = Config.IdCampoNumeroSiniestro },
                new Campo() { Id = Config.IdCampoEstatusSiniestro },
                new Campo() { Id = Config.IdCampoFechaLiquidacionSiniestro, Tipo = TipoCampo.Fecha },
                new Campo() { Id = Config.IdCampoTipoProveedor },
                new Campo() { Id = Config.IdCampoCodigoProveedor },
                new Campo() { Id = Config.IdCampoCedulaProveedor },
                new Campo() { Id = Config.IdCampoNombreProveedor },
                new Campo() { Id = Config.IdCampoNumeroFactura },
                new Campo() { Id = Config.IdCampoFechaLiquidacionFactura, Tipo = TipoCampo.Fecha },
                new Campo() { Id = Config.IdCampoEstatusFactura }
            };
        }

        private void ActualizarValores(ref IList<Campo> campos, Siniestro siniestro)
        {
            //En orden
            campos[0].Valor = siniestro.Empresa;
            campos[1].Valor = siniestro.Sucursal;
            campos[2].Valor = siniestro.Ramo;
            campos[3].Valor = siniestro.NombreAsegurado;
            campos[4].Valor = siniestro.CedulaAsegurado;
            campos[5].Valor = siniestro.NumeroPoliza;
            campos[6].Valor = siniestro.TipoSiniestro;
            campos[7].Valor = siniestro.FechaOcurrenciaSiniestro.Valor;
            campos[8].Valor = siniestro.NumeroSiniestro;
            campos[9].Valor = siniestro.EstatusSiniestro;
            campos[10].Valor = siniestro.FechaLiquidacionSiniestro.Valor;
            campos[11].Valor = siniestro.TipoProveedor;
            campos[12].Valor = siniestro.CodigoProveedor;
            campos[13].Valor = siniestro.CedulaProveedor;
            campos[14].Valor = siniestro.NombreProveedor;
            campos[15].Valor = siniestro.NumeroFactura;
            campos[16].Valor = siniestro.FechaLiquidacionFactura.Valor;
            campos[17].Valor = siniestro.EstatusFactura;
        }
    }
}