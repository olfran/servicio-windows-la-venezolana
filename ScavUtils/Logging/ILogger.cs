﻿namespace Logging
{
    /// <summary>
    /// Interfaz simple que permite realizar logs según su nivel
    /// </summary>
    public interface ILogger
    {
        bool Log(string message);
    }
}
