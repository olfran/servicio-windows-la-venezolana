﻿using LaVenezolana.Properties;
using LaVenezolana.Services;
using System;
using System.ServiceProcess;
using System.Timers;

namespace LaVenezolana
{
    /// <summary>
    /// Este servicio permite automatizar el sistema de registro de siniestros
    /// de la empresa "La Venezolana de Seguros"
    /// 
    /// El código se encuentra estructurado en forma de componentes para que sea más fácil
    /// su mantenimiento o extensión.
    /// 
    /// Se ejecuta con una frecuencia configurable -en minutos- (Mediante un Timer)
    /// Si ocurre algún error se generan los logs correspondientes y se sigue
    /// ejecutando el servicio con total normalidad.
    /// 
    /// Por Olfran Jiménez <olfran@gmail.com>
    /// </summary>

    public partial class MainService : ServiceBase
    {
        Timer _timer;
        int _elapsedMinutesCrear = 0;
        int _elapsedMinutesAdjuntar = 0;

        public MainService()
        {
            InitializeComponent();
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Elapsed");
            DateTime now = DateTime.Now;
            _elapsedMinutesCrear++;
            _elapsedMinutesAdjuntar++;

            var actualizarTime = ParseTime(Settings.Default.ActualizarSiniestrosIntervalo);

            
            //Tiempo de espera en minutos para chequear la actualización de siniestros, sólo minutos impares
            if (_elapsedMinutesCrear >= Settings.Default.CrearSiniestrosIntervaloMinutos && IsOdd(now.Minute))
            {
                CrearSiniestros();
            }

            //Actualizar
            if (now.Hour == actualizarTime.Item1 && now.Minute == actualizarTime.Item2)
            {
                ActualizarSiniestros();
            }

            //Tiempo de espera en minutos para chequear adjuntar
            if (_elapsedMinutesAdjuntar >= Settings.Default.AdjuntarArchivosFaxIntervaloMinutos)
            {
                AdjuntarArchivosFax();
            }
        }

        /// <summary>
        /// Crear los nuevos siniestros a partir del archivo txt cada 20 minutos por defecto, 
        /// pero puede ser configurado desde el app.config
        /// </summary>
        void CrearSiniestros()
        {
            var siniestros = new LaVenezolanaMain();

            _elapsedMinutesCrear = 0;
            siniestros.CrearSiniestros();
        }

        /// <summary>
        /// Actualizar los siniestros a partir del archivo txt el intervalo 
        /// puede ser configurado desde el app.config
        /// </summary>
        void ActualizarSiniestros()
        {
            var siniestros = new LaVenezolanaMain();

            siniestros.ActualizarSiniestros();
        }

        /// <summary>
        /// Adjuntar los archivos de FAX
        /// </summary>
        void AdjuntarArchivosFax()
        {
            var siniestros = new LaVenezolanaMain();

            _elapsedMinutesAdjuntar = 0;
            siniestros.AdjuntarArchivos();
        }

        protected override void OnStart(string[] args)
        {
            //Ejecutar cada minuto
            _timer = new Timer(1000 * 60);
            _timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            _timer.Start();
        }

        protected override void OnStop()
        {
            _timer.Stop();
        }

        Tuple<int, int> ParseTime(string time)
        {
            try
            {
                string[] splitted = time.Split(':');

                //Horas
                int hours = ParseInt(splitted[0]);
                int minutes = ParseInt(splitted[1]);

                return new Tuple<int, int>(hours, minutes);
            }
            catch (Exception)
            {
                return new Tuple<int, int>(0, 0);
            }
        }

        //Verificar si es impar
        private bool IsOdd(int minute)
        {
            return minute % 2 != 0;
        }

        private int ParseInt(string n)
        {
            int result = 0;

            int.TryParse(n, out result);

            return result;
        }
    }
}