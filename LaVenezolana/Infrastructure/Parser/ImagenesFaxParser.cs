﻿using LaVenezolana.Infrastructure.DataAccess;
using LaVenezolana.Models;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace LaVenezolana.Infrastructure.Parser
{
    public class ImagenesFaxParser : IParser<ImagenesFax>
    {
        readonly IFaxFilesFinder _filesFinder;

        public ImagenesFaxParser(IFaxFilesFinder filesFinder)
        {
            _filesFinder = filesFinder;
        }

        public IList<ImagenesFax> Parse()
        {
            IList<ImagenesFax> result = new List<ImagenesFax>();

            var files = _filesFinder.SearchFiles();

            foreach (var item in files)
            {
                var numSiniestro = FindNumeroSiniestro(item.Xml);

                result.Add(new ImagenesFax
                {
                    ArchivosFax = item,
                    NumeroSiniestro = numSiniestro
                });
            }


            return result;
        }

        /// <summary>
        /// Retorna el número de siniestro del archivo XML
        /// </summary>
        /// <param name="xml">Path al archivo XML</param>
        /// <returns>Número de siniestro o string vacío si no se encontró</returns>
        string FindNumeroSiniestro(string pathXml)
        {
            try
            {
                var xdoc = XDocument.Load(pathXml);

                return xdoc
                    .Element("FaxIndexes")
                    .Element("FaxMetaData")
                    .Elements("Index").First()
                    .Element("Value").Value;
            }
            catch
            {
                return "";
            }
        }
    }
}
