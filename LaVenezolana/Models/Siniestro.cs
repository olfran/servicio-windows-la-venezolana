﻿using ScavUtils.Infrastructure.Models;

namespace LaVenezolana.Models
{
    /// <summary>
    /// Representa un modelo de siniestro
    /// </summary>
    public class Siniestro : BaseModel
    {
        public string Empresa { get; set; }
        public string Sucursal { get; set; }
        public string Ramo { get; set; }
        public string NombreAsegurado { get; set; }
        public string CedulaAsegurado { get; set; }
        public string NumeroPoliza { get; set; }
        public string TipoSiniestro { get; set; }
        public Campo FechaOcurrenciaSiniestro { get; set; }
        public string NumeroSiniestro { get; set; }
        public string EstatusSiniestro { get; set; }
        public Campo FechaLiquidacionSiniestro { get; set; }
        public string TipoProveedor { get; set; }
        public string CodigoProveedor { get; set; }
        public string CedulaProveedor { get; set; }
        public string NombreProveedor { get; set; }
        public string NumeroFactura { get; set; }
        public Campo FechaLiquidacionFactura { get; set; }
        public string EstatusFactura { get; set; }

        public Siniestro()
        {
            FechaOcurrenciaSiniestro = new Campo()
            {
                Tipo = TipoCampo.Fecha
            };

            FechaLiquidacionSiniestro = new Campo()
            {
                Tipo = TipoCampo.Fecha
            };

            FechaLiquidacionFactura = new Campo()
            {
                Tipo = TipoCampo.Fecha
            };
        }
    }
}
