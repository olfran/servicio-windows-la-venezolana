﻿using LaVenezolana.Infrastructure.DataAccess;
using LaVenezolana.Models;
using System;
using System.Collections.Generic;

namespace LaVenezolana.Infrastructure.Parser
{
    /// <summary>
    /// Convierte datos de un archivo txt de creación al modelo de Siniestro
    /// Sólo se llenan los datos del Número de Siniestro y de la Cédula porque
    /// es lo único que aparece en el archivo de creación
    /// </summary>
    public class CreacionParser : IParser<Siniestro>
    {
        //Separador de los campos en el archivo de texto, por defecto 4 espacios
        private const string Separador = " ";

        private ISiniestrosTextReader _reader;

        /// <summary>
        /// Sólo se llenan los datos del Número de Siniestro y de la Cédula porque
        /// es lo único que aparece en el archivo de creación
        /// </summary>
        /// <param name="reader">Stream de donde leer los datos</param>
        public CreacionParser(ISiniestrosTextReader reader)
        {
            _reader = reader;
        }

        /// <summary>
        /// Analizar el archivo txt y convertirlo a una lista de Siniestro
        /// </summary>
        /// <returns>Retorna la lista de siniestros contenida en el archivo txt o una lista vacía si hubo error</returns>
        public IList<Siniestro> Parse()
        {
            IList<Siniestro> result = new List<Siniestro>();

            try
            {
                string line = _reader.ReadLine();

                while (!String.IsNullOrEmpty(line))
                {
                    var siniestro = ParseLine(line);

                    if (!String.IsNullOrEmpty(siniestro.NumeroSiniestro))
                    {
                        result.Add(siniestro);
                    }

                    line = _reader.ReadLine();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Convierte una línea a un modelo Siniestro
        /// </summary>
        /// <param name="line">Línea a procesar</param>
        /// <returns></returns>
        private Siniestro ParseLine(string line)
        {
            string[] pieces = line.Trim().Split(new string[] { Separador }, StringSplitOptions.RemoveEmptyEntries);

            if (pieces.Length != 2)
            {
                ThrowException();
            }

            return new Siniestro()
            {
                CedulaAsegurado = pieces[0],
                NumeroSiniestro = pieces[1],
                TextLine = line
            };
        }

        public void ThrowException()
        {
            throw new InvalidOperationException("Al parecer el archivo de texto de Creación tiene un formato inválido, deberían haber 2 columnas separadas entre sí por espacios, por favor consulte el diccionario de datos");
        }
    }
}
