﻿using System.Collections.Generic;

namespace ScavUtils.Infrastructure.Models
{
    public class Documento
    {
        public long Id { get; set; }
        public long IdCatalogo { get; set; }
        public string Numref { get; set; }
        public IList<Campo> Campos { get; set; }
        public uint TotalAdjuntos { get; set; }
        public long IdFlujo { get; set; }
    }
}