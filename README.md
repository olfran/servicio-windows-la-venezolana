# Windows service para parsing periódico de archivos txt
### Por Olfran Jiménez <olfran@gmail.com>

##Descripción
Servicio de Windows que corre en segundo plano y ejecuta tareas de Parsing de varios archivos de texto que contienen datos de interés y permite convertirlos a datos compatibles con el software SCAV/AIRE del Grupo Archicentro.

## Herramientas/Tecnologías utilizadas
* .NET Framework 4.5
* SOLID
* Programming to Interfaces
* Multilayered
* Unit Testing (In progress)