﻿namespace ScavConfiguration
{
    public interface IScavConfigurationManager
    {
        string Get(string itemName);
    }
}