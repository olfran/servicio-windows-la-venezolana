﻿using LaVenezolana.Models;
using System.Collections.Generic;

namespace LaVenezolana.Infrastructure.DataAccess
{
    /// <summary>
    /// Buscar los archivos XML y TIF generados por el FAX recursivamente
    /// a partir de un directorio base
    /// </summary>
    public interface IFaxFilesFinder
    {
        /// <summary>
        /// Busca los archivos generados por el FAX recursivamente
        /// </summary>
        /// <returns>Lista de archivos o lista vacía si falló</returns>
        IList<FaxFiles> SearchFiles();
    }
}