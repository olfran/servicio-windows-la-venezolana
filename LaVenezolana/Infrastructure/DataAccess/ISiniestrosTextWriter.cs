﻿using System;

namespace LaVenezolana.Infrastructure.DataAccess
{
    /// <summary>
    /// Escribir a un archivo de texto
    /// </summary>
    public interface ISiniestrosTextWriter : IDisposable
    {
        /// <summary>
        /// Escribir una línea de texto
        /// </summary>
        /// <param name="line">Línea a escribir</param>
        void WriteLine(string line);

        /// <summary>
        /// Cierra el stream
        /// </summary>
        void Close();
    }
}