﻿namespace ScavUtils.Infrastructure.Models.Types
{
    /// <summary>
    /// Tipos de formularios en SCAV pertinentes a RedEmpleo
    /// </summary>
    public struct TipoFormulario
    {
        public const int Transcripcion = 0;
        public const int Etiqueta = 3;
        public const int FlujoTrabajo = 5;
        public const int Correo = 7;
    }
}
