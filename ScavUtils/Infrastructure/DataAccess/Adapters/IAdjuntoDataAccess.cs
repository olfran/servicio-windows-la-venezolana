﻿using System.Collections.Generic;
using ScavUtils.Infrastructure.Models;

namespace ScavUtils.Infrastructure.DataAccess.Adapters
{
    public interface IAdjuntoDataAccess
    {
        /// <summary>
        /// Retorna todos los adjuntos pertenecientes al documento y al usuario especificado
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <returns>Lista o lista vacía si no hay ningún adjunto</returns>
        IList<AdjuntoPropiedades> GetAdjuntos(long idCatalogo, long idDocumento);

        /// <summary>
        /// Encuentra un adjunto
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <param name="idAdjunto">Id adjunto</param>
        /// <returns>Datos del adjunto</returns>
        AdjuntoPropiedades FindAdjunto(long idCatalogo, long idDocumento, long idAdjunto);

        /// <summary>
        /// Adjunta el primer archivo al documento
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="idCatalogo">ID Catálogo</param>
        /// <param name="idDocumento">ID Documento</param>
        /// <param name="idUsuario">ID Usuario</param>
        /// <returns>true o false</returns>
        bool AdjuntarPrimerArchivo(string path, long idCatalogo, long idDocumento, long idUsuario);
    }
}