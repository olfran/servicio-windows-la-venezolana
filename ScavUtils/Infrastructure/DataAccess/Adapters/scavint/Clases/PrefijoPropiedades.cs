﻿namespace scavint.Clases
{
    public class PrefijoPropiedades
    {
        public long idPrefijo { get; set; }
        public string nombrePrefijo { get; set; }
        public int digitosAno { get; set; }
        public int digitosConsecutivo { get; set; }
        public long idCatalogo { get; set; }
    }
}