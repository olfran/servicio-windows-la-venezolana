﻿using LaVenezolana.Models;
using System.Collections.Generic;
using System.IO;

namespace LaVenezolana.Infrastructure.DataAccess
{
    /// <summary>
    /// Buscar los archivos XML y TIF generados por el FAX recursivamente
    /// a partir de un directorio base
    /// </summary>
    public class FaxFilesFinder : IFaxFilesFinder
    {
        readonly string _baseDir;

        /// <summary>
        /// Constructor que acepta el directorio base donde buscar recursivamente
        /// los archivos XML y TIF
        /// </summary>
        /// <param name="baseDir"></param>
        public FaxFilesFinder(string baseDir)
        {
            _baseDir = baseDir;
        }

        /// <summary>
        /// Busca los archivos generados por el FAX recursivamente
        /// </summary>
        /// <returns>Lista de archivos o lista vacía si falló</returns>
        public IList<FaxFiles> SearchFiles()
        {
            IList<FaxFiles> result = new List<FaxFiles>();

            var xmls = Directory.EnumerateFiles(_baseDir, "*.xml", SearchOption.AllDirectories);

            foreach (var xml in xmls)
            {
                result.Add(new FaxFiles
                {
                    Xml = xml,
                    Tif = GetTifPath(xml)
                });
            }

            return result;
        }

        /// <summary>
        /// Retorna el path del archivo TIF
        /// </summary>
        /// <param name="xml">Path completo del archivo XML</param>
        /// <returns>Path del archivo TIF</returns>
        private string GetTifPath(string xml)
        {
            string fileName = Path.GetFileNameWithoutExtension(xml);
            string dir = Path.GetDirectoryName(xml);

            return Path.Combine(dir, fileName + ".tif");
        }
    }
}