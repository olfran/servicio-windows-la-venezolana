﻿namespace Logging.Backend
{
    public interface ILogBackend
    {
        bool Log(string message);
    }
}
