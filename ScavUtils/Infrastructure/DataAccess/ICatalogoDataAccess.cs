﻿using System.Collections.Generic;
using ScavUtils.Infrastructure.Models;

namespace ScavUtils.Infrastructure.DataAccess
{
    public interface ICatalogoDataAccess
    {
        /// <summary>
        /// Retorna una lista de todos los catálogos a los cuales el usuario tiene acceso
        /// </summary>
        /// <param name="idUsuario">Id del usuario</param>
        /// <returns>Lista de catálogos o lista vacía si hubo error o no tiene acceso a ningún catálogo</returns>
        IList<CatalogoPropiedades> GetCatalogos(long idUsuario);

        /// <summary>
        /// Verifica si un usuario tiene acceso al catálogo
        /// </summary>
        /// <param name="idUsuario">Id de usuario</param>
        /// <param name="idCatalogo">Id del catálogo a verificar</param>
        /// <returns>true si tiene acceso de lo contrario false</returns>
        bool TieneAcceso(long idUsuario, long idCatalogo);

        /// <summary>
        /// Devuelve los diferentes tipos de directorios asociados al catálogo
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <returns>AdjuntoDirectorios</returns>
        AdjuntoDirectorios GetDirectorios(long idCatalogo);
    }
}