﻿using System;
using System.Collections.Generic;
using System.Data;
using ScavUtils.Infrastructure.Models;

namespace ScavUtils.Infrastructure.DataAccess
{
    /// <summary>
    /// Utilidades para trabajar con SQL para SCAV
    /// </summary>
    public interface ISQLCommonsScav
    {
        /// <summary>
        /// Ejecutar una sentencia SQL
        /// </summary>
        /// <param name="sql">Sentencia SQL</param>
        /// <param name="parametros">Opcional. Diccionario con los parámetros para la consulta</param>
        bool ExecuteQuery(string sql, Dictionary<string, object> parametros, Action<bool, IDataReader> callback);
        
        /// <summary>
        /// Ejecutar una sentencia SQL
        /// </summary>
        /// <param name="sql">Sentencia SQL</param>
        /// <param name="parametros">Opcional. Diccionario con los parámetros paraa la consulta</param>
        bool ExecuteNonQuery(string sql, Dictionary<string, object> parametros = null);
        
        /// <summary>
        /// Ejecuta y devuelve un valor escalar, retorna -1 si hubo error
        /// </summary>
        object ExecuteScalar(string sql, Dictionary<string, object> parametros = null);
        
        /// <summary>
        /// Retorna los campos de una lista de nombres de campos separados por comas. Ej. CAMPO_1,CAMPO_2,CAMPO_3
        /// </summary>
        string GetCamposSeparados(IList<Campo> campos, bool incluirVacios = false);
        
        /// <summary>
        /// Retorna el SQL INSERT al Catálogo indicado con los campos dinámicos y los parámetros 
        /// correspondientes en forma de ? ideal para ser utilizado en conjunto con los métodos
        /// GetParametros y ExecuteNonQuery
        /// </summary>
        string GetInsert(IList<Campo> campos, long idCatalogo, string numref, bool incluirVacios = false);

        /// <summary>
        /// Retorna un diccionario de parámetros NombreCampo / Valor
        /// </summary>
        /// <param name="campos">Campos con sus respectivos valores</param>
        /// <param name="valor">Opcional: Valor a utilizar en lugar de los valores de los campos</param>
        /// <param name="comenzarDesde">Valor desde el cual comenzar a construir los parámetros</param>
        /// <param name="incluirVacios">Incluir o no los campos vacíos</param>
        /// <returns>Diccionario de parámetros NombreCampo / Valor</returns>
        Dictionary<string, object> GetParametros(IList<Campo> campos, string valor = "", int comenzarDesde = 1, bool incluirVacios = false);

        /// <summary>
        /// Retorna los campos de una lista de Parámetros separados por comas. Ej. ?,?,?
        /// </summary>
        string GetParametrosSeparados(IList<Campo> campos, bool incluirVacios = false);
        
        /// <summary>
        /// Retorna el SQL SELECT al Catálogo indicado y opcionalmente (si idDocumento > 0) al Documento 
        /// </summary>
        string GetSelect(IList<Campo> campos, long idCatalogo, long idDocumento = 0, bool incluirFrom = true);
        
        /// <summary>
        /// Retorna la sentencia where separada por comas junto con los respectivos parámetros
        /// en forma de ?, ejemplo WHERE CAMPO_1=? AND
        /// Opcionalmente se puede especificar el operador lógico a utilizar por defecto AND
        /// </summary>
        /// <param name="campos">Campos a buscar</param>
        /// <param name="operadorLogico">Operador lógico entre AND o OR</param>
        /// <param name="incluirWhere">Si se incluye o no la palabra WHERE de SQL, ideal para filtros</param>
        string GetWhere(IList<Campo> campos, string operadorLogico = "AND", bool incluirWhere = true, bool incluirVacios = false);

        /// <summary>
        /// Retorna la sentencia where separada por comas junto con los respectivos parámetros
        /// en forma de ?, ejemplo WHERE CAMPO_1 LIKE '%' + ? + '%' AND
        /// Opcionalmente se puede especificar el operador lógico a utilizar por defecto AND
        /// </summary>
        /// <param name="campos">Campos a buscar</param>
        /// <param name="operadorLogico">Operador lógico entre AND o OR</param>
        /// <param name="incluirWhere">Si se incluye o no la palabra WHERE de SQL, ideal para filtros</param>
        string GetWhereLike(IList<Campo> campos, string operadorLogico = "AND", bool incluirWhere = true, bool incluirVacios = false);

        /// <summary>
        /// Retorna el SQL para la paginación, sólo funciona en SQL Server 2012
        /// </summary>
        /// <param name="orderBy">
        /// bool: Si se anexa order by a la consulta, ya que es requerido por SQL Server
        /// </param>
        string GetSqlPaginador(bool orderBy = true);

        /// <summary>
        /// Convierte un valor string a Date
        /// </summary>
        /// <param name="str">Valor a convertir</param>
        /// <returns>Valor convertido o DateTime.Now si hubo error</returns>
        DateTime? ParseDate(string str);

        /// <summary>
        /// Convierte un valor string a long
        /// </summary>
        /// <param name="number">Valor a convertir</param>
        /// <returns>Valor convertido o 0 si hubo error</returns>
        long ParseLong(string number);

        /// <summary>
        /// Retorna el SQL UPDATE al Catálogo indicado con los campos dinámicos y los parámetros 
        /// correspondientes en forma de ? ideal para ser utilizado en conjunto con los métodos
        /// GetParametros y ExecuteNonQuery
        /// </summary>
        string GetUpdate(IList<Campo> campos, long idCatalogo, long idDocumento);

        /// <summary>
        /// Retorna el SQL UPDATE al Catálogo indicado con los campos dinámicos para el Where y los parámetros 
        /// correspondientes en forma de ? ideal para ser utilizado en conjunto con los métodos
        /// GetParametros y ExecuteNonQuery
        /// </summary>
        string GetUpdate(IList<Campo> campos, long idCatalogo, IList<Campo> camposWhere);

        /// <summary>
        /// Paginador
        /// </summary>
        Paginador Paginador { get; set; }

        /// <summary>
        /// Retorna el ID del primer usuario de SCAV
        /// </summary>
        /// <returns></returns>
        long GetIdPrimerUsuario();

        /// <summary>
        /// Normaliza las fechas a formato ISO para SQL
        /// </summary>
        void NormalizarFechas(IList<Campo> campos);

        /// <summary>
        /// Mapea un DataReader a una lista de campos
        /// </summary>
        IList<Campo> MapReaderToCampos(IList<Campo> campos, IDataReader reader);

        string ConnectionString { get; }
    }
}
