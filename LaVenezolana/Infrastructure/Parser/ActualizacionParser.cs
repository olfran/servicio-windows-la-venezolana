﻿using LaVenezolana.Infrastructure.DataAccess;
using LaVenezolana.Models;
using System;
using System.Collections.Generic;

namespace LaVenezolana.Infrastructure.Parser
{
    /// <summary>
    /// Convierte datos del archivo txt de actualización
    /// </summary>
    public class ActualizacionParser : IParser<Siniestro>
    {
        //Separador de los campos en el archivo de texto, por defecto 4 espacios
        private const string Separador = "   ";
        private const int MaxColumnas = 18;
        private ISiniestrosTextReader _reader;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="reader">Stream de donde leer los datos</param>
        public ActualizacionParser(ISiniestrosTextReader reader)
        {
            _reader = reader;
        }

        /// <summary>
        /// Analizar el archivo txt y convertirlo a una lista de Siniestro
        /// </summary>
        /// <returns>Retorna la lista de registros contenida en el archivo txt o una lista vacía si hubo error</returns>
        public IList<Siniestro> Parse()
        {
            IList<Siniestro> result = new List<Siniestro>();

            try
            {
                string line = _reader.ReadLine();

                while (!String.IsNullOrEmpty(line))
                {
                    var parsed = ParseLine(line);

                    if (parsed != null)
                    {
                        result.Add(parsed);
                    }

                    line = _reader.ReadLine();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Convierte una línea a un modelo Registro
        /// </summary>
        /// <param name="line">Línea a procesar</param>
        /// <returns></returns>
        private Siniestro ParseLine(string line)
        {
            Siniestro registro = new Siniestro();

            registro.TextLine = line;

            string[] pieces = line.Trim().Split(new string[] { Separador }, StringSplitOptions.RemoveEmptyEntries);

            if (pieces.Length == MaxColumnas)
            {
                //En orden
                registro.Empresa = pieces[0].TrimStart(' ');
                registro.Sucursal = pieces[1].TrimStart(' ');
                registro.Ramo = pieces[2].TrimStart(' ');
                registro.NombreAsegurado = pieces[3].TrimStart(' ');
                registro.CedulaAsegurado = pieces[4].TrimStart(' ');
                registro.NumeroPoliza = pieces[5].TrimStart(' ');
                registro.TipoSiniestro = pieces[6].TrimStart(' ');
                registro.FechaOcurrenciaSiniestro.Valor = pieces[7].TrimStart(' ');
                registro.NumeroSiniestro = pieces[8].TrimStart(' ');
                registro.EstatusSiniestro = pieces[9].TrimStart(' ');
                registro.FechaLiquidacionSiniestro.Valor = pieces[10].TrimStart(' ');
                registro.TipoProveedor = pieces[11].TrimStart(' ');
                registro.CodigoProveedor = pieces[12].TrimStart(' ');
                registro.CedulaProveedor = pieces[13].TrimStart(' ');
                registro.NombreProveedor = pieces[14].TrimStart(' ');
                registro.NumeroFactura = pieces[15].TrimStart(' ');
                registro.FechaLiquidacionFactura.Valor = pieces[16].TrimStart(' ');
                registro.EstatusFactura = pieces[17].TrimStart(' ');

                return registro;
            }

            return null;
        }

        public void ThrowException()
        {
            throw new InvalidCastException($"Al parecer el archivo de texto de Actualización tiene un formato inválido, deberían haber 18 columnas separadas entre sí por espacios, por favor consulte el diccionario de datos");
        }
    }
}
