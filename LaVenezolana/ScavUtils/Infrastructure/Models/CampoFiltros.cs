﻿using System.Collections.Generic;
using ScavUtils.Infrastructure.Models;

namespace ScavUtils.Infrastructure.Models
{
    /// <summary>
    /// Relación entre un campo y sus filtros
    /// </summary>
    public class CampoFiltros
    {
        public Campo Campo { get; set; }
        /// <summary>
        /// Lista de filtros
        /// </summary>
        public IList<Filtro> Filtros { get; set; }
    }
}