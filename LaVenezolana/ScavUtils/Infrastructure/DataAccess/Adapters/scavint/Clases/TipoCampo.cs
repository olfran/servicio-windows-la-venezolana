﻿namespace scavint.Clases
{
    public static class TipoCampo
    {
        public const int Texto = 0;
        public const int Numero = 1;
        public const int Fecha = 2;
        public const int Memo = 3;
        public const int Imagen = 4;
        public const int Etiqueta = 5;
        public const int Numref = 8;
        public const int Vista = 10;
        
    }
}