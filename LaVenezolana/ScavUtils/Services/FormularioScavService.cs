﻿using System.Collections.Generic;
using ScavUtils.Infrastructure.Models;
using ScavUtils.Infrastructure.DataAccess.Adapters;

namespace ScavUtils.Services
{
    /// <summary>
    /// Trabajar con Formularios Scav
    /// </summary>
    public class FormularioScavService : IFormularioScavService
    {
        private long _idCatalogo;
        private IScavAdapter _scav;

        public FormularioScavService(long idCatalogo, IScavAdapter scavAdapter)
        {
            _idCatalogo = idCatalogo;
            _scav = scavAdapter;
        }

        /// <summary>
        /// Retorna los campos de un formulario dependiendo del tipo, o una lista vacía
        /// si hay error
        /// </summary>
        public IList<Campo> GetCamposFormulario(int tipoFormulario)
        {
            return _scav.GetCamposFormulario(_idCatalogo, tipoFormulario);
        }
    }
}