﻿using ScavUtils.Infrastructure.Models;
using System;
using System.Text.RegularExpressions;

namespace ScavUtils.Validators
{
    /// <summary>
    /// Valida Números usando una expresión regular
    /// </summary>
    public class Numero : IValidator
    {
        public bool IsValid(Campo campo)
        {
            bool resultado = true;
            if (!String.IsNullOrEmpty(campo.Valor))
            {
                string regex = @"\d+";
                resultado = Regex.IsMatch(campo.Valor, regex);

                if (!resultado)
                {
                    campo.Mensajes.Add("Por favor introduzca un valor numérico");
                }
            }

            return resultado;
        }
    }
}
