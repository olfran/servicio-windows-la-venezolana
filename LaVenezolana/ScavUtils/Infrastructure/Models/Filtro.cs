﻿namespace ScavUtils.Infrastructure.Models
{
    /// <summary>
    /// Modelo para los Filtros de búsqueda de empleo
    /// </summary>
    public class Filtro
    {
        /// <summary>
        /// Contenido del filtro
        /// </summary>
        public string Valor { get; set; }
        /// <summary>
        /// Total de filtros iguales al valor indicado
        /// </summary>
        public long Total { get; set; }
    }
}