﻿using LaVenezolana.Infrastructure.DataAccess;
using LaVenezolana.Infrastructure.Parser;
using LaVenezolana.Models;
using Logging;
using Logging.Backend;
using ScavConfiguration;
using ScavConfiguration.Interfaces;
using ScavConfiguration.Readers;
using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.DataAccess.Adapters;
using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LaVenezolana.Services
{
    /// <summary>
    /// Clase principal del proyecto
    /// Esta clase está acoplada a éste proyecto, no se puede utilizar fuera del mismo
    /// contiene métodos acoplados a las dependencias por defecto del proyecto
    /// </summary>
    public class LaVenezolanaMain
    {
        const string OkAdjuntar = "OK ADJUNTAR - Archivo TIF adjuntado correctamente al documento con Número de Siniestro: [{0}]";
        const string ErrorAdjuntar = "ERROR ADJUNTAR - No se pudo adjuntar el archivo TIF al documento con Número de Siniestro: [{0}]";

        readonly ISQLCommonsScav _sqlCommons;
        readonly IScavDataAccess _scavDataAccess;
        readonly ILogger _logger;
        readonly IAdjuntoDataAccess _adjuntoDataAccess;
        readonly ICatalogoDataAccess _catalogoDataAccess;
        readonly LaVenezolanaConfiguracion _config;
        readonly FlujoTrabajoService _flujoTrabajoService;

        public LaVenezolanaMain()
        {
            _sqlCommons = new SQLCommonsScav(LaVenezolana.Properties.Settings.Default.ConnectionString);
            _scavDataAccess = GetScavDataAccess(_sqlCommons);
            _logger = GetLogger();
            _catalogoDataAccess = new CatalogoDataAccess(_sqlCommons);
            _adjuntoDataAccess = new AdjuntoDataAccess(_sqlCommons, _catalogoDataAccess);
            _flujoTrabajoService = new FlujoTrabajoService(new ScavAdapter(_sqlCommons));
            _config = GetLaVenezolanaConfig();
        }

        /// <summary>
        /// Crear los siniestros en la base de datos de SCAV/AIRE y escribir los logs
        /// </summary>
        public void CrearSiniestros()
        {
            IList<Siniestro> noProcesados = new List<Siniestro>();

            try
            {
                //Bloquear el archivo para que nadie pueda abrirlo
                using (FileStream fileStream = File.Open(Properties.Settings.Default.PathArchivoCreacionSiniestros, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    var service = GetCrearSiniestrosService(fileStream);
                    noProcesados = service.CrearSiniestros();
                }

                //Eliminar las líneas ya procesadas
                EliminarProcesados(Properties.Settings.Default.PathArchivoCreacionSiniestros, noProcesados);
            }
            catch (Exception ex)
            {
                _logger.Log(ex.ToString());
            }
        }

        /// <summary>
        /// Actualizar los siniestros en la base de datos de SCAV/AIRE y escribir los logs
        /// </summary>
        public void ActualizarSiniestros()
        {
            IList<Siniestro> noProcesados = new List<Siniestro>();

            try
            {
                //Bloquear el archivo para que nadie pueda abrirlo
                using (FileStream fileStream = File.Open(Properties.Settings.Default.PathArchivoActualizacionSiniestros, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    var service = GetActualizarSiniestrosService(fileStream);

                    noProcesados = service.ActualizarSiniestros();
                }

                //Eliminar las líneas ya procesadas
                EliminarProcesados(Properties.Settings.Default.PathArchivoActualizacionSiniestros, noProcesados);
            }
            catch (Exception ex)
            {
                _logger.Log(ex.ToString());
            }
        }

        /// <summary>
        /// Adjunta los archivos del FAX y los envía al flujo
        /// </summary>
        public void AdjuntarArchivos()
        {
            try
            {
                string baseDir = LaVenezolana.Properties.Settings.Default.PathDirectorioFax;
                IFaxFilesFinder fax = new FaxFilesFinder(baseDir);
                IParser<ImagenesFax> parser = new ImagenesFaxParser(fax);
                IList<ImagenesFax> filesFax = parser.Parse();
                long idFlujo = LaVenezolana.Properties.Settings.Default.IdFlujoTrabajo;
                long idUsuario = _sqlCommons.GetIdPrimerUsuario();

                foreach (ImagenesFax file in filesFax)
                {
                    List<Campo> campos = new List<Campo>
                    {
                        new Campo() { Id = _config.IdCampoNumeroSiniestro, Valor = file.NumeroSiniestro }
                    };

                    var documentos = _scavDataAccess.Buscar(campos, _config.IdCatalogoSiniestros);

                    if (documentos.Count > 0)
                    {
                        long idDocumento = documentos.First().Id;
                        bool okAdjuntar = _adjuntoDataAccess.AdjuntarTiff(file.ArchivosFax.Tif, _config.IdCatalogoSiniestros, idDocumento, idUsuario);

                        if (okAdjuntar)
                        {
                            try
                            {
                                File.Move(file.ArchivosFax.Xml, Path.Combine(LaVenezolana.Properties.Settings.Default.PathCarpetaProcesados, Guid.NewGuid().ToString() + ".xml"));
                                _logger.Log(String.Format(OkAdjuntar, file.NumeroSiniestro));
                            }
                            catch(Exception ex)
                            {
                                _logger.Log(ex.ToString());
                            }
                        }
                        else
                        {
                            _logger.Log(String.Format(ErrorAdjuntar, file.NumeroSiniestro));
                        }

                        //Enviar el documento al flujo
                        if (_flujoTrabajoService.EnviarDocumento(_config.IdCatalogoSiniestros, idDocumento, idFlujo, idUsuario))
                        {
                            _logger.Log(String.Format("OK FLUJO - Enviado al Flujo correctamente. Id Documento: {0} - NumeroSiniestro: {1} - ArchivoFax: {2}", idDocumento, file.NumeroSiniestro, file.ArchivosFax.Xml));
                        }
                        else
                        {
                            _logger.Log(String.Format("ERROR FLUJO - Id Documento: {0} - NumeroSiniestro: {1} - ArchivoFax: {2}", idDocumento, file.NumeroSiniestro, file.ArchivosFax.Xml));
                        }
                    }
                    else
                    {
                        _logger.Log(String.Format("OK FLUJO - Documento no encontrado. NumeroSiniestro: {0} - ArchivoFax: {1}", file.NumeroSiniestro, file.ArchivosFax.Xml));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Log(ex.ToString());
            }
        }

        private CrearSiniestrosService GetCrearSiniestrosService(FileStream fileStream)
        {
            ISiniestrosTextReader siniestrosTextReader = GetSiniestrosTextReader(fileStream);
            IParser<Siniestro> creacionParser = new CreacionParser(siniestrosTextReader);

            return new CrearSiniestrosService(creacionParser, _scavDataAccess, _logger)
            {
                Config = _config
            };
        }

        private ActualizarSiniestrosService GetActualizarSiniestrosService(FileStream fileStream)
        {
            ISiniestrosTextReader siniestrosTextReader = GetSiniestrosTextReader(fileStream);
            IParser<Siniestro> actualizacionParser = GetActualizacionParser(siniestrosTextReader);

            return new ActualizarSiniestrosService(actualizacionParser, _scavDataAccess, _logger)
            {
                Config = _config
            };
        }

        /// <summary>
        /// Eliminar del archivo de texto los registros ya procesados (dejar sólo los no procesados)
        /// El proceso debe tener permisos de lectura escritura a dicho archivo
        /// </summary>
        /// <typeparam name="T">T Lista de registros a escribir</typeparam>
        /// <param name="noProcesados">Registros no procesados</param>
        /// <param name="fileName">Nombre del archivo</param>
        private void EliminarProcesados<T>(string fileName, IList<T> noProcesados) where T : BaseModel
        {
            using (FileStream fileStream = File.Open(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (ISiniestrosTextWriter writer = new SiniestrosTextWriter(fileStream))
                {
                    foreach (T item in noProcesados)
                    {
                        writer.WriteLine(item.TextLine);
                    }
                }
            }
        }

        /*****************************************************************************************
         * A partir de aquí están las Dependencias por defecto del proyecto                      *
         * Se pueden sustituir facilmente implementando las interfaces correspondientes          *
         *****************************************************************************************/

        //Logger
        private ILogger GetLogger()
        {
            ILogBackend logBackend = new FileBackend(LaVenezolana.Properties.Settings.Default.LogsPath, true);

            return new Logger(logBackend);
        }

        //ConfigurationManager
        private ScavConfigurationManager GetConfigurationManager()
        {
            IScavConfigurationReader configurationReader =
                new ScavWebConfigurationReader(LaVenezolana.Properties.Settings.Default.ConnectionString);

            return new ScavConfigurationManager(configurationReader);
        }

        //ScavDataAccess
        private IScavDataAccess GetScavDataAccess(ISQLCommonsScav sqlCommons)
        {
            IScavAdapter scavAdapter = new ScavAdapter(sqlCommons);

            return new ScavDataAccess(sqlCommons, scavAdapter);
        }

        //Parser
        private IParser<Siniestro> GetCreacionParser(ISiniestrosTextReader siniestrosTextReader)
        {
            return new CreacionParser(siniestrosTextReader);
        }

        private IParser<Siniestro> GetActualizacionParser(ISiniestrosTextReader siniestrosTextReader)
        {
            return new ActualizacionParser(siniestrosTextReader);
        }

        private ISiniestrosTextReader GetSiniestrosTextReader(FileStream fileStream)
        {
            return new SiniestrosTextReader(fileStream);
        }

        private LaVenezolanaConfiguracion GetLaVenezolanaConfig()
        {
            return new LaVenezolanaConfiguracion()
            {
                IdCatalogoSiniestros = Properties.Settings.Default.IdCatalogoSiniestros,
                IdCampoCedulaAsegurado = Properties.Settings.Default.IdCampoCedulaAsegurado,
                IdCampoCedulaProveedor = Properties.Settings.Default.IdCampoCedulaProveedor,
                IdCampoCodigoProveedor = Properties.Settings.Default.IdCampoCodigoProveedor,
                IdCampoEmpresa = Properties.Settings.Default.IdCampoEmpresa,
                IdCampoEstatusSiniestro = Properties.Settings.Default.IdCampoEstatusSiniestro,
                IdCampoFechaLiquidacionSiniestro = Properties.Settings.Default.IdCampoFechaLiquidacionSiniestro,
                IdCampoFechaOcurrenciaSiniestro = Properties.Settings.Default.IdCampoFechaOcurrenciaSiniestro,
                IdCampoNombreAsegurado = Properties.Settings.Default.IdCampoNombreAsegurado,
                IdCampoNombreProveedor = Properties.Settings.Default.IdCampoNombreProveedor,
                IdCampoNumeroPoliza = Properties.Settings.Default.IdCampoNumeroPoliza,
                IdCampoNumeroSiniestro = Properties.Settings.Default.IdCampoNumeroSiniestro,
                IdCampoRamo = Properties.Settings.Default.IdCampoRamo,
                IdCampoSucursal = Properties.Settings.Default.IdCampoSucursal,
                IdCampoTipoProveedor = Properties.Settings.Default.IdCampoTipoProveedor,
                IdCampoTipoSiniestro = Properties.Settings.Default.IdCampoTipoSiniestro,
                IdCampoEstatusFactura = Properties.Settings.Default.IdCampoEstatusFactura,
                IdCampoFechaLiquidacionFactura = Properties.Settings.Default.IdCampoFechaLiquidacionFactura,
                IdCampoNumeroFactura = Properties.Settings.Default.IdCampoNumeroFactura
            };
        }
    }
}
