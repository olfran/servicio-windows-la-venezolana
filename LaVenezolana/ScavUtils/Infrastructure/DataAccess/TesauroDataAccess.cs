﻿using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScavUtils.Infrastructure.DataAccess
{
    public class TesauroDataAccess : ITesauroDataAccess
    {
        private ISQLCommonsScav _sqlCommons;

        public TesauroDataAccess(ISQLCommonsScav sqlCommons)
        {
            _sqlCommons = sqlCommons;
        }

        /// <summary>
        /// Retorna una lista de términos o una lista vacía en el modelo Tesauro si hubo error
        /// </summary>
        public Tesauro GetTerminos(long idTesauro)
        {
            Tesauro resultado = new Tesauro();
            string sql = String.Format("SELECT ID_TERMINO,TERMINO_TESAURO FROM TERMINO_{0} ORDER BY TERMINO_TESAURO", idTesauro);

            resultado.ID = idTesauro;

            _sqlCommons.ExecuteQuery(sql, null, (error, reader) =>
            {
                if (!error)
                {
                    try
                    {
                        while (reader.Read())
                        {
                            resultado.Terminos.Add(new Termino()
                            {
                                ID = reader.GetInt64(0),
                                Valor = reader[1].ToString()
                            });
                        }
                    }
                    catch
                    {
                        resultado = new Tesauro();
                    }
                }
            });

            return resultado;
        }
    }
}
