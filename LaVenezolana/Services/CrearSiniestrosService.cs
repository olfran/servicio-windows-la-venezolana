﻿using LaVenezolana.Infrastructure.Parser;
using LaVenezolana.Models;
using Logging;
using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;

namespace LaVenezolana.Services
{
    /// <summary>
    /// Servicio para generar los registros de los siniestros en la base de datos
    /// de SCAV a partir del archivo de texto
    /// </summary>
    public class CrearSiniestrosService
    {
        public LaVenezolanaConfiguracion Config { get; set; }

        private const string OkCrear = "OK CREATE - Documento con Número de Siniestro: [{0}] creado correctamente";
        private const string ErrorCrear = "ERROR CREATE - Documento con Número de Siniestro: [{0}] no pudo ser creado, probablemente ya exista";

        private readonly IParser<Siniestro> _siniestrosParser;
        private readonly IScavDataAccess _scavDataAccess;
        private readonly ILogger _logger;

        /// <summary>
        /// Crear los siniestros en forma de Registros de SCAV
        /// </summary>
        public CrearSiniestrosService(IParser<Siniestro> siniestrosParser, IScavDataAccess scavDataAccess, ILogger logger)
        {
            _siniestrosParser = siniestrosParser;
            _scavDataAccess = scavDataAccess;
            _logger = logger;
        }

        /// <summary>
        /// Crear los siniestros y almacenar en el log la operación
        /// </summary>
        /// <returns>Los registros no procesados o una lista vacía si todos fueron procesados correctamente</returns>
        public IList<Siniestro> CrearSiniestros()
        {
            IList<Siniestro> siniestros = _siniestrosParser.Parse();
            IList<Siniestro> noProcesados = new List<Siniestro>();
            IList<Campo> campos = new List<Campo>
            {
                new Campo() { Id = Config.IdCampoCedulaAsegurado },
                new Campo() { Id = Config.IdCampoNumeroSiniestro }

            };

            foreach (var siniestro in siniestros)
            {
                if (!SiniestroExiste(siniestro.NumeroSiniestro))
                {
                    campos[0].Valor = siniestro.CedulaAsegurado;
                    campos[1].Valor = siniestro.NumeroSiniestro;

                    if (_scavDataAccess.CrearDocumento(campos, Config.IdCatalogoSiniestros) > 0)
                    {
                        string ok = String.Format(OkCrear, siniestro.NumeroSiniestro);
                        _logger.Log(ok);
                    }
                    else
                    {
                        noProcesados.Add(siniestro);
                        string error = String.Format(ErrorCrear, siniestro.NumeroSiniestro);
                        _logger.Log(error);
                    }
                }
            }

            return noProcesados;
        }

        private bool SiniestroExiste(string numeroSiniestro)
        {
            //Parche no optimizado, pero que más dá
            IList<Campo> filtros = new List<Campo>()
            {
                new Campo
                {
                    Id = Config.IdCampoNumeroSiniestro,
                    Valor = numeroSiniestro
                }
            };

            return _scavDataAccess.GetTotal(Config.IdCatalogoSiniestros, filtros) > 0;
        }
    }
}