﻿namespace LaVenezolana.Infrastructure.DataAccess
{
    /// <summary>
    /// Leer de un archivo de texto
    /// </summary>
    public interface ISiniestrosTextReader
    {
        /// <summary>
        /// Leer una línea de texto
        /// </summary>
        /// <returns>Línea de texto leída</returns>
        string ReadLine();
    }
}