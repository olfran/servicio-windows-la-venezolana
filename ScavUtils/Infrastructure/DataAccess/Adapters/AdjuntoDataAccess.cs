﻿using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Transactions;

namespace ScavUtils.Infrastructure.DataAccess.Adapters
{
    public class AdjuntoDataAccess : IAdjuntoDataAccess
    {
        readonly ISQLCommonsScav _sqlCommons;
        readonly ICatalogoDataAccess _catalogoDataAccess;

        public AdjuntoDataAccess(ISQLCommonsScav sqlCommons, ICatalogoDataAccess catalogoDataAccess)
        {
            _sqlCommons = sqlCommons;
            _catalogoDataAccess = catalogoDataAccess;
        }

        /// <summary>
        /// Retorna todos los adjuntos pertenecientes al documento
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <returns>Lista o lista vacía si no hay ningún adjunto</returns>
        public IList<AdjuntoPropiedades> GetAdjuntos(long idCatalogo, long idDocumento)
        {
            IList<AdjuntoPropiedades> resultado = new List<AdjuntoPropiedades>();
            string joinCatalogo = String.Format(" JOIN CATALOGO_{0} ON CATALOGO_{0}.ID_DOCUMENTO=ADJUNTOS_C_{0}.ID_DOCUMENTO", idCatalogo);
            string sqlWhere = String.Format(" WHERE FECHA_ELIMINACION IS NULL AND ADJUNTOS_C_{0}.ID_DOCUMENTO={1}", idCatalogo, idDocumento);
            string sqlOrderBy = " ORDER BY FECHA_ASOCIACION ASC";
            string sql = String.Format("SELECT ADJUNTOS_C_{0}.NOMBRE_ARCH,ADJUNTOS_C_{0}.ID_ADJUNTO,ADJUNTOS_C_{0}.NOMBRE_PANTA,ADJUNTOS_C_{0}.EXT FROM ADJUNTOS_C_{0} {1} JOIN USUARIO ON USUARIO.ID_USUARIO=ADJUNTOS_C_{0}.ID_USUARIO ", idCatalogo, joinCatalogo);

            _sqlCommons.ExecuteQuery(sql + sqlWhere + sqlOrderBy, null, (error, reader) =>
            {
                if (!error)
                {
                    while (reader.Read())
                    {
                        resultado.Add(new AdjuntoPropiedades()
                        {
                            Id = _sqlCommons.ParseLong(reader[1].ToString()),
                            Nombre = reader[2].ToString(),
                            NombreFisico = reader[0].ToString(),
                            Extension = reader[3].ToString()
                        });

                    }
                }
            });

            return resultado;
        }

        /// <summary>
        /// Encuentra un adjunto
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <param name="idAdjunto">Id adjunto</param>
        /// <returns>Datos del adjunto</returns>
        public AdjuntoPropiedades FindAdjunto(long idCatalogo, long idDocumento, long idAdjunto)
        {
            AdjuntoPropiedades resultado = new AdjuntoPropiedades();
            string joinCatalogo = String.Format(" JOIN CATALOGO_{0} ON CATALOGO_{0}.ID_DOCUMENTO=ADJUNTOS_C_{0}.ID_DOCUMENTO", idCatalogo);
            string sqlWhere = String.Format(" WHERE FECHA_ELIMINACION IS NULL AND ADJUNTOS_C_{0}.ID_DOCUMENTO={1} AND ADJUNTOS_C_{0}.ID_ADJUNTO={2}", idCatalogo, idDocumento, idAdjunto);
            string sqlOrderBy = " ORDER BY FECHA_ASOCIACION ASC";
            string sql = String.Format("SELECT ADJUNTOS_C_{0}.NOMBRE_ARCH,ADJUNTOS_C_{0}.ID_ADJUNTO,ADJUNTOS_C_{0}.NOMBRE_PANTA,ADJUNTOS_C_{0}.EXT FROM ADJUNTOS_C_{0} {1} JOIN USUARIO ON USUARIO.ID_USUARIO=ADJUNTOS_C_{0}.ID_USUARIO ", idCatalogo, joinCatalogo);

            _sqlCommons.ExecuteQuery(sql + sqlWhere + sqlOrderBy, null, (error, reader) =>
            {
                if (!error)
                {
                    reader.Read();
                    resultado.Id = _sqlCommons.ParseLong(reader[1].ToString());
                    resultado.Nombre = reader[2].ToString();
                    resultado.NombreFisico = reader[0].ToString();
                    resultado.Extension = reader[3].ToString();
                }
            });

            return resultado;
        }

        /// <summary>
        /// Adjunta el primer archivo al documento
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="idCatalogo">ID Catálogo</param>
        /// <param name="idDocumento">ID Documento</param>
        /// <param name="idUsuario">ID Usuario</param>
        /// <returns>true o false</returns>
        public bool AdjuntarPrimerArchivo(string path, long idCatalogo, long idDocumento, long idUsuario)
        {
            string sql = String.Format("INSERT INTO ADJUNTOS_C_{0}(ID_DOCUMENTO,EXT,PAGINA,NOMBRE_ARCH,NOMBRE_PANTA,FECHA_ASOCIACION,FECHA_MODIFICACION,TAMANIO_ARCHIVO,ID_USUARIO) VALUES({1},?,?,?,?,?,?,?,{2})", idCatalogo, idDocumento, idUsuario);
            string sqlQuetiene = String.Format("UPDATE CATALOGO_{0} SET QUETIENE='01000000' WHERE ID_DOCUMENTO={1}", idCatalogo, idDocumento);
            FileInfo fileInfo = new FileInfo(path);
            string nombreArch = GetNombreArch(TipoAdjunto.Archivo, idCatalogo, idDocumento);

            Dictionary<string, object> parametros = new Dictionary<string, object>
            {
                {"@p1", Path.GetExtension(path).Substring(1)},
                {"@p2", 1},
                {"@p3", nombreArch},
                {"@p4", Path.GetFileName(path)},
                {"@p5", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
                {"@p6", File.GetLastWriteTime(path).ToString("yyyy-MM-dd HH:mm:ss")},
                {"@p7", fileInfo.Length},
            };

            string dirArchivos = _catalogoDataAccess.GetDirectorios(idCatalogo).Archivo;
            string newPath = Path.Combine(GetDirectoriosPorId(idDocumento, dirArchivos), nombreArch);
            bool ok = true;

            using (TransactionScope scope = new TransactionScope())
            {
                if (!File.Exists(newPath))
                {
                    File.Move(path, newPath);
                }

                try
                {
                    ok = _sqlCommons.ExecuteNonQuery(sql, parametros);
                    _sqlCommons.ExecuteNonQuery(sqlQuetiene);

                    scope.Complete();
                }
                catch (Exception)
                {
                    ok = false;
                }
            }

            if (!ok)
            {
                if (!File.Exists(newPath))
                {
                    File.Move(newPath, path);
                }
            }

            return ok;
        }

        /// <summary>
        /// Método sacado del código fuente de AIRE
        /// </summary>
        string GetDirectoriosPorId(long idDocumento, string rutaBase)
        {
            if (rutaBase.Substring(rutaBase.Length - 1, 1) != "\\") { rutaBase = rutaBase + "\\"; }
            string stringId3Digitos = idDocumento.ToString().PadLeft(3, '0');
            if (stringId3Digitos.Length > 3) { stringId3Digitos = stringId3Digitos.Substring(stringId3Digitos.Length - 3); }
            if (!Directory.Exists(rutaBase + "D" + stringId3Digitos.Substring(0, 1))) { Directory.CreateDirectory(rutaBase + "D" + stringId3Digitos.Substring(0, 1)); }
            if (!Directory.Exists(rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1))) { Directory.CreateDirectory(rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1)); }
            if (!Directory.Exists(rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1) + "\\D" + stringId3Digitos.Substring(2, 1))) { Directory.CreateDirectory(rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1) + "\\D" + stringId3Digitos.Substring(2, 1)); }
            return rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1) + "\\D" + stringId3Digitos.Substring(2, 1) + "\\";
        }

        string GetNombreArch(int tipoAdjunto, long idCatalogo, long idDocumento, int pagina = 1)
        {
            string nombre = "";
                switch (tipoAdjunto)
                {
                    case TipoAdjunto.Imagen:
                        nombre = "I_CA_" + idCatalogo + "_" + idDocumento + "_" + pagina;
                        break;
                    case TipoAdjunto.Archivo:
                        nombre = "A_CA_" + idCatalogo + "_" + idDocumento + "_" + pagina;
                        break;
                    case TipoAdjunto.Multimedia:
                        nombre = "M_CA_" + idCatalogo + "_" + idDocumento + "_" + pagina;
                        break;
                    case TipoAdjunto.Texto:
                        nombre = "T_CA_" + idCatalogo + "_" + idDocumento + "_" + pagina;
                        break;
                    case TipoAdjunto.Web:
                        nombre = "W_CA_" + idCatalogo + "_" + idDocumento + "_" + pagina;
                        break;
                }
            return nombre;
        }

        class TipoAdjunto
        {
            public const int Imagen = 0;
            public const int Archivo = 1;
            public const int Multimedia = 2;
            public const int Texto = 3;
            public const int Web = 4;
            public const int FT = 5;
            public const int Confidencial = 6;
        }
    }
}
