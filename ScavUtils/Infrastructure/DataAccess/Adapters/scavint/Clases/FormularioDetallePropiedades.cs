﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace scavint.Clases
{
    /// <summary>
    /// get / set para el detalle de cada campo del formulario, nombre, posición, tipo de elemnto, etc.
    /// 
    /// </summary>
    public class FormularioDetallePropiedades
    {
        public long _idCampo { get; set; }
        public string _nombreCampo { get; set; }
        public int _tipoElemento { get; set; }
        public long _posY { get; set; }
        public long _posX { get; set; }
        public long _tamY { get; set; }
        public long _tamX { get; set; }
        public int _visible { get; set; }
        public int _Habilitado { get; set; }
        public int _MaxLenght { get; set; }
        public bool _asuntoTitulo { get; set; }
        public bool _variosTerminos { get; set; }
        public long _idTesauro { get; set; }
        public string _formato { get; set; }
        public string _fuenteNombre { get; set; }
        public string _fuenteColor { get; set; }
        public string _fuenteTamano { get; set; }
        /// <summary>
        /// Tipo de elemento definido para la etiqueta en la tabla formulario_detalle (2 registros por campo, la etiqueta del campo es independiente)
        /// </summary>
        public int _tipoElementoEtiqueta { get; set; }
        public bool _obligatorio { get; set; }
        public bool _Autollenado { get; set; }
        public string ValorDefecto { get; set; }
        public bool CodigoBarra { get; set; }
    }
}