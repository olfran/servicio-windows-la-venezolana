﻿using System.Collections.Generic;

namespace LaVenezolana.Infrastructure.Parser
{
    /// <summary>
    /// Convierte datos al modelo de Siniestro o Archivos del FAX
    /// </summary>
    public interface IParser<T>
    {
        IList<T> Parse();
    }
}