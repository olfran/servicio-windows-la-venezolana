﻿using System.IO;

namespace LaVenezolana.Infrastructure.DataAccess
{
    /// <summary>
    /// Escribir a un archivo de texto
    /// </summary>
    public class SiniestrosTextWriter : ISiniestrosTextWriter
    {
        private StreamWriter _writer;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileStream">Archivo a escribir</param>
        public SiniestrosTextWriter(FileStream fileStream)
        {
            _writer = new StreamWriter(fileStream);
        }

         /// <summary>
        /// Escribir una línea de texto
        /// </summary>
        /// <param name="line">Línea a escribir</param>
        public void WriteLine(string line)
        {
            _writer.WriteLine(line);
            _writer.Flush();
        }

        /// <summary>
        /// Cierra el stream
        /// </summary>
        public void Close()
        {
            _writer.Dispose();
        }

        public void Dispose()
        {
            Close();
        }
    }
}