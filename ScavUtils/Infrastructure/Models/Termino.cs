﻿namespace ScavUtils.Infrastructure.Models
{
    /// <summary>
    /// Representa un término
    /// </summary>
    public class Termino
    {
        public long ID { get; set; }
        public string Valor { get; set; }
    }
}
