﻿namespace LaVenezolana.Models
{
    /// <summary>
    /// Representa archivos XML y TIF del FAX
    /// </summary>
    public class FaxFiles
    {
        public string Xml { get; set; }
        public string Tif { get; set; }
    }
}
