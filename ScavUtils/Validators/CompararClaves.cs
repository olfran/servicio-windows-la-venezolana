﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace ScavUtils.Validators
{
    /// <summary>
    /// Compara si dos valores son iguales, por supuesto es Case Sensitive
    /// </summary>
    public class CompararClaves : IValidator
    {
        IList<Campo> _campos;

        /// <summary>
        /// Comparar si dos valores son iguales,
        /// </summary>
        /// <param name="campos">
        /// Lista de campos contra los cuales comparar
        /// </param>
        public CompararClaves(IList<Campo> campos)
        {
            _campos = campos;
        }

        public bool IsValid(Campo campo)
        {
            try
            {
                //Esperamos una fórmula tipo @repiteclave campo_123
                string nombreCampo2 = campo.ValorDefecto.Split(' ')[1].ToLower();

                //Buscar el campo a comparar
                Campo campo2 = _campos.Where(x => x.Nombre == nombreCampo2).FirstOrDefault();
                bool resultado = campo.Valor == campo2.Valor;

                if (!resultado)
                {
                    campo.Mensajes.Add("Las contraseñas no coinciden");
                }

                return resultado;
            }
            catch
            {
                return true;
            }

            return true;
        }
    }
}
